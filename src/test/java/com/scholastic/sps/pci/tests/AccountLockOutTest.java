package com.scholastic.sps.pci.tests;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.jayway.restassured.path.xml.XmlPath;


import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;

public class AccountLockOutTest extends BaseTest
{
	final String ENDPOINT_PCICOMPLIANCE_AUTHENTICATION="/AuthenticatePCI/processRemote";
	
	String clientID;
	String isSingleToken;
	String userName;
	String password;
	String xmlResponse;
	//bkukrin@juno1.com
	//ewood@juno1.com
	
	
	@Test
	public void ExistingUserAccountLockOutForInvalidPasswordTest()throws InterruptedException
	{
		System.out.println("\n\n\n\n******* Test:RestAssured - ExistingUserAccountLockOutForInvalidPasswordTest() ***********");
		
		clientID="\"COOL\"";
		isSingleToken="\"true\"";
		//userName="\"tangle@juno1.com\"";
		//userName="\"jrangle@juno1.com\"";
		//userName=" \"ewood@juno1.com\" ";
		userName="\"sps_qa1455815171855@sample.com\"";
		
		password="\"passe\"";
		int i=1;
		
		XmlPath xmlPath = null; 
		for(i=1;i<11;i++)
		{
			Thread.sleep(4000L);
			ExtractableResponse<Response> existingUserLoginResponse=
								given()
										.contentType("application/x-www-form-urlencoded")
										.param("SPSWSXML",createQueryParam()).
								when()
										.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
								then()
										.statusCode(HttpStatus.OK.value())
										.contentType("application/xml")
										.extract();
			   
			xmlResponse=existingUserLoginResponse.xmlPath().getString("");
			System.out.println(xmlResponse);

			xmlPath = new XmlPath(xmlResponse);
			xmlPath.setRoot("SchWS");
			if(xmlPath.getInt("attribute[1].@value")==210)
			{
				break;
			}
			System.out.println("*****"+xmlPath.get("attribute[0].@value"));
			System.out.println("****"+xmlPath.get("attribute[1].@value"));
		}
		
			Assert.assertTrue(xmlPath.get("attribute[0].@value").equals("You have exceeded maximum number of invalid sign in attempts. For your security, your account has been locked and you must wait 30 minutes before attempting to sign in again. You may also reset your password to clear the lockout."));
			Assert.assertTrue("Number Of Log in Attempt:", i==10);
	}
	
	public String createQueryParam()
	{
		String queryParam=
				"<SchWS>"+""
				+"<attribute "+"name="+"\"clientID\""+" value="+clientID+"/>"+
				"<attribute name="+"\"isSingleToken\"" +" value="+isSingleToken+"/>"+
				"<attribute name="+"\"userName\"" +" value="+userName+"/>"+
				"<attribute name="+"\"password\"" +" value="+password+"/>"+
				"</"+"SchWS"+">";
		System.out.println("++++++++++++"+queryParam);
		return queryParam;
	}
}	