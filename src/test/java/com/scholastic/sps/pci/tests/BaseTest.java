package com.scholastic.sps.pci.tests;

import static com.jayway.restassured.config.RestAssuredConfig.config;
import static com.jayway.restassured.config.SSLConfig.sslConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import com.jayway.restassured.RestAssured;

public class BaseTest 
{
public static String testENV;
public static FileInputStream fis=null;
public static Properties prop=null;
public static String qaPropfilePath=System.getProperty("user.dir")+"/src/test/resources/qa.properties";
public static String devPropfilePath=System.getProperty("user.dir")+"/src/test/resources/dev.properties";

@BeforeClass
public static void b4()
{
		/*RestAssured.baseURI="https://schws.qa1.scholastic.com";
		//RestAssured.baseURI="http://schws.dev2.scholastic.net";
		RestAssured.port=8080;
		//RestAssured.port=80;
		RestAssured.basePath="/SchWS/services/SPS";
		RestAssured.config = config().sslConfig(sslConfig().allowAllHostnames());
		testENV=("qa");
		//testENV=("dev");*/
		 		 		
	
		RestAssured.baseURI=System.getProperty("RestAssured.baseURI");
		RestAssured.port=Integer.parseInt(System.getProperty("RestAssured.port"));
		RestAssured.basePath=System.getProperty("RestAssured.basePath");
		RestAssured.config = config().sslConfig(sslConfig().allowAllHostnames());
		testENV=System.getProperty("TestEnvironment");
		
		
		prop=new Properties();
		try
		{
				if(testENV.equalsIgnoreCase("dev"))
				{
					fis=new FileInputStream(devPropfilePath);
					prop.load(fis);
				}else if(testENV.equalsIgnoreCase("qa"))
				{
					fis=new FileInputStream(qaPropfilePath);
					prop.load(fis);
				}
		}catch(IOException e)	
		{
			System.out.println("File Not Found :"+ e.getMessage());	
		}
		
	}	
	
	@Before
	public void initialization() 
	{
		System.out.println("\n\n\n\n*******Initialized RestAssured*******");
		System.out.println("RestAssured.baseURI: " + RestAssured.baseURI);
		System.out.println("RestAssured.port: " + RestAssured.port);
		System.out.println("RestAssured.basePath: " + RestAssured.basePath); 
		System.out.println("******************************\n\n\n\n");
	}
	
	@Rule 
    public TestWatcher watchman = new TestWatcher() 
    {
		@Override
		protected void succeeded(Description description) 
		{
			System.out.println("Test :"+ description.getMethodName()  +" succeeded.");
		}

		@Override
		protected void failed(Throwable e, Description description) 
		{
			System.out.println("Test :"+ description.getMethodName() + " failed with " + e.getMessage() + ".");
		}

		@Override
		protected void starting(Description description) 
		{
			System.out.println("Beginning Test :" + description.getMethodName());
		}
    };
	
	public String readProperty(String key)
	{
		return prop.getProperty(key);
	}

}