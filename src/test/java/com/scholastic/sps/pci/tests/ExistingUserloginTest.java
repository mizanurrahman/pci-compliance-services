package com.scholastic.sps.pci.tests;
import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.path.xml.XmlPath;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;

public class ExistingUserloginTest extends BaseTest
{
	String clientID;
	String isSingleToken;
	String userName;
	String password;
	
	final String ENDPOINT_PCICOMPLIANCE_AUTHENTICATION="/AuthenticatePCI/processRemote";
	

	@Test
	public void existingUserLoginWithNonPciCompliancePasswordTest() 
	{
		System.out.println("\n\n\n\n******* Test:RestAssured - existingUserLoginWithNonPciCompliancePasswordTest() *******");
		
		clientID=" \"COOL\" ";
		isSingleToken=" \"true\" ";
		userName=" \""+readProperty("nonpciComplianceUser")+"\" ";
		password=" \""+readProperty("nonpciCompliancePassword")+"\" ";
		
		ExtractableResponse<Response> existingUserLoginResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(HttpStatus.OK.value())
									.contentType("application/xml")
									.extract();
			   
		String xxx=existingUserLoginResponse.xmlPath().getString("");
		XmlPath xmlPath = new XmlPath(xxx);
		xmlPath.setRoot("SchWS");
		System.out.println(xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").equals("Password must be minimum of 7 and maximum of 30 characters, should contain at least an alphabetic letter and a number.Spaces are not allowed."));
	}
	
	
	@Test
	public void existingUserLoginWithPciCompliancePasswordTest() 
	{
		System.out.println("\n\n\n\n******* Test:RestAssured - existingUserLoginWithPciCompliancePasswordTest() *******");
		
		clientID=" \"COOL\" ";
		isSingleToken=" \"true\" ";
		userName=" \""+readProperty("pciComplianceUser")+"\" ";
		password=" \""+readProperty("pciCompliancePassword")+"\" ";
		System.out.println(userName);
		ExtractableResponse<Response> existingUserLoginResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(HttpStatus.OK.value())
									.contentType("application/xml")
									.extract();
			   
		String xmlResponse=existingUserLoginResponse.xmlPath().getString("");
		System.out.println(xmlResponse);
		XmlPath xmlPath = new XmlPath(xmlResponse);
		xmlPath.setRoot("SchWS");
		System.out.println("**************************"+xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").toString().contains("z2Ao58jstU6fUOpAZpgacA=="));
		//Assert.assertTrue(xmlPath.get("attribute[0].@value").toString().contains("Hu6FqRqYT4qfUOpAZpgacA=="));
	}
	
	@Test
	public void existingUserLogWithInvalidUserNameTest()
	{
		System.out.println("\n\n\n\n******* Test:RestAssured - existingUserLogWithInvalidUserNameTest() *******");
		
		clientID=" \"COOL\" ";
		isSingleToken=" \"true\" ";
		userName=" \"jbari@sample.com\" ";
		password=" \"passed1\" ";
		
		ExtractableResponse<Response> existingUserLoginResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(HttpStatus.OK.value())
									.contentType("application/xml")
									.extract();
			   
		String xmlResponse=existingUserLoginResponse.xmlPath().getString("");
		System.out.println(xmlResponse);
		XmlPath xmlPath = new XmlPath(xmlResponse);
		xmlPath.setRoot("SchWS");
		System.out.println("**************************"+xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").toString().contains("The account information you entered does not match our records."));
	}
	
	@Test
	public void existingUserLogWithInvalidPasswordTest()
	{
		System.out.println("\n\n\n\n******* Test:RestAssured - existingUserLogWithInvalidPasswordTest() *******");
		
		clientID=" \"COOL\" ";
		isSingleToken=" \"true\" ";
		userName=" \""+readProperty("pciComplianceUser")+"\" ";
		//userName=" \"jbari@juno1.com\" ";
		//password=" \"pass1\" ";
		//userName=" \"tgreen12@juno1.com\" ";
		password=" \"pass15678\" ";
		
		ExtractableResponse<Response> existingUserLoginResponse=
							given()
									.contentType("application/x-www-form-urlencoded")
									.param("SPSWSXML",createQueryParam()).
							when()
									.get(ENDPOINT_PCICOMPLIANCE_AUTHENTICATION).
							then()
									.statusCode(HttpStatus.OK.value())
									.contentType("application/xml")
									.extract();
			   
		String xmlResponse=existingUserLoginResponse.xmlPath().getString("");
		System.out.println(xmlResponse);
		XmlPath xmlPath = new XmlPath(xmlResponse);
		xmlPath.setRoot("SchWS");
		System.out.println("**************************"+xmlPath.get("attribute[0].@value"));
		Assert.assertTrue(xmlPath.get("attribute[0].@value").toString().contains("The account information you entered does not match our records."));
	}
	
	public String createQueryParam()
	{
		String queryParam=
				"<SchWS>"+
				"<attribute "+"name="+"\"clientID\" "+" value="+clientID+"/>"+
				"<attribute name="+"\"isSingleToken\" " +" value="+isSingleToken+"/>"+
				"<attribute name="+"\"userName\" " +" value="+userName+"/>"+
				"<attribute name="+"\"password\" " +" value="+password+"/>"+
				"</"+"SchWS"+">";
		return queryParam;
	}
}